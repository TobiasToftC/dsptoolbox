function [  ] = arduinoFilterIIR( z,p,k )
%arduinoFilterIIR   Loads an Arduino skecth and edits the contents to
%represent the chosen filter. The program takes the input and converts it
%to a system of second order IIR sections. The sections are formatted to
%the Arduino syntax and the differens equations are setup as well. The
%outcome is an edited Arduino file that can be run without further user
%interact.
% 
%USAGE
%     arduinoFilterIIR( z,p,k );
% 
%INPUT ARGUMENTS
%       z : Zeros of filter (i.e use butter() or cheby())
%       p : Poles of filter (i.e use butter() or cheby())
%       k : Gain of filter (i.e use butter() or cheby())
% 
%OUTPUT ARGUMENTS
%       Does not return anything. Uploads variables and coefficients to
%       Arduino.
% 
%EXAMPLE
% n = 10;                     % Order of the filter
% fs = 44100;                 % Sampling frequency [Hz]
% f_pass = 600;               % Passband frequency
% f_stop = 12000;             % Stopband frequency
% 
% Wh = (fs/(2*pi))*0.5;       % Normalized sampling frequenzy [rad/s] 
% Wp = (f_pass/(2*pi))/Wh;    % Normalized passband freq
% Ws = (f_stop/(2*pi))/Wh;    % Normalized stopband freq
% 
% % 3 options for Wn:
% % # 1: Only [Wp] = Lowpass filter
% % # 2: Only [Ws] = Highpass filter
% % # 3: Both [Wp Ws] = Bandpass filter
% 
% Wn = [Wp];                  % Frquency vector
% 
% % Calulates the zeros, poles and the gain for the filter
% [z,p,k] = butter(n,Wn);
%
% % Uploads to Arduino:
% arduinoFilterIIR( z,p,k );
% 
%   Authors  :  Mikkel Heber Hahn Petersen - s123028@student.dtu.dk
%              Tobias Toft Christensen - s123028@student.dtu.dk
%              Technical University of Denmark (DTU)
%
%   History :
%   v.1.0   28/01/2016
%   ***********************************************************************

% Importing packages to automately launch and run Arduino IDE
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
robot=Robot;

% Getting the Arduino code skecth.
fid = fopen('./../arduinoFilterArduinoCode/Const_sampling_sketch/Const_sampling_sketch.ino','r+');
i = 1;
tline = fgetl(fid);
A{i} = tline;
while ischar(tline)
    i = i+1;
    tline = fgetl(fid);
    A{i} = tline;
end
fclose(fid);

%% Calculating SOS
b = [];
a = [];

% Defines the length of the delay line.
N = 1024;
% The scaling constant, edit to alter the precision of the filter
% coefficients
divisor = 10000;

% Converts the z,p,k representation to SOS-
[s] = zp2sos(z,p,k,'up','two');

% Used to format the filter coefficients.
[r, c] = size(s);
n = r*3;

%% Formating b and a coefficients
% Extracts the filter coefficietns from the SOS matrix.
for i = 1:r
    b = [b s(i,1:3)];
    a = [a s(i,4:6)];
end

% Scales the coefficients
b = round(b*divisor);
a = round(a*divisor);

% Formatting
B_coef = sprintf('volatile int b[%d] = {',3*r);
A_coef = sprintf('volatile int a[%d] = {',3*r);

% Stores the filter coefficients to one array with the Arduino formatting.
for i = 1:length(b)
    B_coef = sprintf('%s%d',B_coef,b(i));
    A_coef = sprintf('%s%d',A_coef,a(i));
    if i < length(b)
        B_coef = sprintf('%s,',B_coef);
        A_coef = sprintf('%s,',A_coef);
    end
end
% The filter coefficients as a string that can be compiles by the Arduino
% IDE.
B_coef = sprintf('%s};',B_coef);
A_coef = sprintf('%s};',A_coef);

%% Writing the parameters
% Rewrites the new coefficients and variables to the Arduino sketch. Bee
% carefull not to alter anything in the sketch without changeing the line
% numbers here aswell. where A{x} is x'th line. 
A{24} = sprintf('#define m %d',N);
A{26} = sprintf('#define n %d',N-1);
A{27} = sprintf(B_coef);
A{28} = sprintf(A_coef);
A{31} = sprintf('int divisor = %d;',divisor);

A_1 = A(1:30);
A_2 = A(31:end);

for i = 1:r
  Abuf{i} = sprintf('int x%d[m];',i);
end
A = [A_1 Abuf A_2];

A_1 = A(1:47+r);
A_2 = A(48+r:end);

%% Difference equation
% Formattes the difference equation so the Arduino IDE can compile.
DFnb = sprintf('x1[j] = (b[0]*x[j] + b[1]*x[(m+j-1)%%m] + b[2]*x[(m+j-2)%%m]'); 
DFna = sprintf(' - a[1]*x1[(m+j-1)%%m] - a[2]*x1[(m+j-2)%%m])/divisor;');
DF_n = [DFnb DFna];

Abuf{1} = DF_n; 
for i = 2:r
DFn_b = sprintf('x%d[j] = (b[%d]*x%d[j] + b[%d]*x%d[(m+j-1)%%m] + b[%d]*x%d[(m+j-2)%%m]',i,3*i-3,i-1,3*i-3+1,i-1,3*i-3+2,i-1); 
  DFn_a = sprintf(' - a[%d]*x%d[(m+j-1)%%m] - a[%d]*x%d[(m+j-2)%%m])/divisor;',3*i-3+1,i,3*i-3+2,i);
  DFn = [DFn_b DFn_a];
  Abuf{i} = DFn;  
end  
if r > 1
    Abuf{i+1} = sprintf('val_out = x%d[j];',i);  
    A = [A_1 Abuf A_2];
else
    Abuf{2} = sprintf('val_out = x%d[j];',1);  
    A = [A_1 Abuf A_2];
end
    


fid = fopen('./../arduinoFilterArduinoCode/Const_sampling_IIR/Const_sampling_IIR.ino', 'w');
for i = 1:numel(A)
    if A{i+1} == -1
        fprintf(fid,'%s', A{i});
        break
    else
        fprintf(fid,'%s\n', A{i});
    end
end
fclose('all');

%% Launch Arduino IDE and uplode code to Ardiono

% This part enables the Matlab script to automatly upload the skecth to the
% Arduino. Work on Pc and MAC.
a = pwd;
if (ispc)     
    A = ['START ' a '\..\arduinoFilterArduinoCode\Const_sampling_IIR\Const_sampling_IIR.ino'];
    dos(A);
    
    pause(3)
    robot.keyPress(KeyEvent.VK_CONTROL);
    robot.keyPress(KeyEvent.VK_U);
    robot.keyRelease(KeyEvent.VK_CONTROL);
    robot.keyRelease(KeyEvent.VK_U);
else (ismac)
    A = ['open ./../arduinoFilterArduinoCode/Const_sampling_IIR/Const_sampling_IIR.ino'];
    system(A);
    
     pause(3)
    robot.keyPress(KeyEvent.VK_META);
    robot.keyPress(KeyEvent.VK_U);
    robot.keyRelease(KeyEvent.VK_META);
    robot.keyRelease(KeyEvent.VK_U);
end


end