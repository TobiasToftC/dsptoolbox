%write_to_ardiono   Writes filter coefficients and variables to an
%   Arduino file. Use the set up here to get an idea of the use. The three
%   functions can be used without this script, but the inputs must be
%   formatted in the same way as here.
%
%
%USAGE
%     Run the section with the desired type of filter, out-comment
%     everything else.
%
%INPUT ARGUMENTS
%     None.
%
%OUTPUT ARGUMENTS
%     None.
%
%   Authors  :  Mikkel Heber Hahn Petersen - s123028@student.dtu.dk
%              Tobias Toft Christensen - s123028@student.dtu.dk
%              Technical University of Denmark (DTU)
%
%   History :
%   v.1.0   28/01/2016
%   ***********************************************************************

close all
clear
clc



% ********************* USER INPUT **********************
n = 20;                     % Order of the filter
fs = 44100;                 % Sampling frequency [Hz]
f_pass = 3000;              % Passband frequency
f_stop = 5000;              % Stopband frequency
% *******************************************************

Wh = (fs/(2*pi))*0.5;       % Normalized sampling frequenzy [rad/s]
Wp = (f_pass/(2*pi))/Wh;    % Normalized passband freq
Ws = (f_stop/(2*pi))/Wh;    % Normalized stopband freq

% 3 options:
% # 1: Only [Wp] = Lowpass filter
% # 2: Only [Ws] = Highpass filter
% # 3: Both [Wp Ws] = Bandpass filter

Wn = [Wp];                 % Frquency vector

% Calulates the zeros, poles and the gain for the filter
[z,p,k] = butter(n,Wn);
%[b,a] = butter(n,Wn);
[s] = zp2sos(z,p,k);
%  [r, c] = size(s);
%
%     b = [s(2,1:3)];
%     a = [s(2,4:6)];
%
%     isstable(b,a)
%
%     zplane(b,a)

% figure(1)
% freqz(b,a)
% fvtool(s)

%% IIR
% Converts the (z,p,k) to a second order system (SOS) and writes it to the
% arduino file
 arduinoFilterIIR( z,p,k )

%% FIR

order = 116;
imp_length = order+1;
Gains = [-30 -30 -15 -15 0 0 15 20];

H = [1 ones(1,order/2)];

cuts = 1;

% Calulating corner freqs:
for ii = 1:length(Gains)
    cuts(ii+1) = ceil(ii/length(Gains)*length(H));
    
    % Assining gain to bands
    H(cuts(ii):cuts(ii+1)) = db2mag(Gains(ii));
end

% Symmetric spectra
H = [H conj(fliplr(H(2:end)))];

% figure(1)
% plot(H)

hx = ifft(H);

%hanx = [zeros(1,order/4) hann(order/2+1)' zeros(1,order/4)];
hx = fftshift(hx);
%hx = hx.*hanx;

n = 10;
b = ones(1,n+1);
b = b/length(b);
% arduinoFilterFIR( hx );

%% Delay
% arduinoFilterDelay( 0.5, 0.5 );
