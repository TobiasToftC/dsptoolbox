function [  ] = arduinoFilterFIR( b )
%arduinoFilterFIR   Loads an Arduino skecth and edits the contents to
%represent the chosen filter.  are formatted to
%the Arduino syntax and the differens equations are setup as well. The
%outcome is an edited Arduino file that can be run without further user
%interact.
% 
%USAGE
%     arduinoFilterIIR( b );
% 
%INPUT ARGUMENTS
%       b : Filter coefficients. 
% 
%OUTPUT ARGUMENTS
%       Does not return anything. Uploads variables and coefficients to
%       Arduino. 
%
%   Authors  :  Mikkel Heber Hahn Petersen - s123028@student.dtu.dk
%              Tobias Toft Christensen - s123028@student.dtu.dk
%              Technical University of Denmark (DTU)
%
%   History :
%   v.1.0   28/01/2016
%   ***********************************************************************

% Importing packages to automately launch and run Arduino IDE
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
robot=Robot;

% Getting the Arduino code skecth.
fid = fopen('./../arduinoFilterArduinoCode/Const_sampling_sketch/Const_sampling_sketch.ino','r+');
i = 1;
tline = fgetl(fid);
A{i} = tline;
while ischar(tline)
    i = i+1;
    tline = fgetl(fid);
    A{i} = tline;
end
fclose(fid);

%% Initial formatting of the filter coefficients

% The scaling constant, edit to alter the precision of the filter
% coefficients
divisor = 10000;

r = length(b);

% Defines the length of the delay line.
N = 2^nextpow2(r);
%% Formating b coefficients

% Scaling the coefficients.
b = round(b*divisor);

B_coef = sprintf('volatile int b[%d] = {',r);

% Stores the filter coefficients to one array with the Arduino formatting.
for i = 1:length(b)
    B_coef = sprintf('%s%d',B_coef,b(i));
    if i < length(b)
        B_coef = sprintf('%s,',B_coef);
    end
end
B_coef = sprintf('%s};',B_coef);

%% Writing the parameters
% Rewrites the new coefficients and variables to the Arduino sketch. Bee
% carefull not to alter anything in the sketch without changeing the line
% numbers here aswell. where A{x} is x'th line. 
A{24} = sprintf('#define m %d',N);
A{26} = sprintf('#define n %d',N-1);
A{27} = sprintf(B_coef);
A{31} = sprintf('int divisor = %d;',divisor);


%% Difference equation
% Formattes the difference equation so the Arduino IDE can compile.
DFnb = [sprintf('  val_out = (%d*x[j]',b(1))];
for i = 2:r
    DFnb =[DFnb sprintf(' + %d*x[(m+j-%d)%%m]',b(i),i-1)]; 
end
DFnb = [DFnb sprintf(')/divisor;')];
A{47} = DFnb;

fid = fopen('./../arduinoFilterArduinoCode/Const_sampling_FIR/Const_sampling_FIR.ino', 'w');
for i = 1:numel(A)
    if A{i+1} == -1
        fprintf(fid,'%s', A{i});
        break
    else
        fprintf(fid,'%s\n', A{i});
    end
end
fclose('all');

%% Launch Arduino IDE and uplode code to Ardiono

% This part enables the Matlab script to automatly upload the skecth to the
% Arduino. Work on Pc and MAC.
a = pwd;
if (ispc)     
    A = ['START ' a '\..\arduinoFilterArduinoCode\Const_sampling_FIR\Const_sampling_FIR.ino'];
    dos(A);
    
    pause(3)
    robot.keyPress(KeyEvent.VK_CONTROL);
    robot.keyPress(KeyEvent.VK_U);
    robot.keyRelease(KeyEvent.VK_CONTROL);
    robot.keyRelease(KeyEvent.VK_U);
else (ismac)
    A = ['open ./../arduinoFilterArduinoCode/Const_sampling_FIR/Const_sampling_FIR.ino'];
    system(A);
    
     pause(3)
    robot.keyPress(KeyEvent.VK_META);
    robot.keyPress(KeyEvent.VK_U);
    robot.keyRelease(KeyEvent.VK_META);
    robot.keyRelease(KeyEvent.VK_U);
end


end