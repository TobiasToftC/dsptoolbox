function [  ] = arduinoFilterDelay( delay_t, a )
%arduinoFilterDelay   Loads an Arduino skecth and edits the contents to
%represent the chosen filter. The outcome is an edited Arduino file that 
%can be run without further user interact.
% 
%USAGE
%     arduinoFilterDelay( delay_t, a );
% 
%INPUT ARGUMENTS
%       delay_t :   Delay time in seconds. Max 0.74
%       a :         Attenuation constant. Range [0;1]
% 
%OUTPUT ARGUMENTS
%       Does not return anything. Uploads variables and coefficients to
%       Arduino.
%
%   Authors  :  Mikkel Heber Hahn Petersen - s123028@student.dtu.dk
%              Tobias Toft Christensen - s123028@student.dtu.dk
%              Technical University of Denmark (DTU)
%
%   History :
%   v.1.0   28/01/2016
%   ***********************************************************************

% Importing packages to automately launch and run Arduino IDE
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

robot=Robot;

% Getting the Arduino code skecth.
fid = fopen('./../arduinoFilterArduinoCode/Const_sampling_delay/Const_sampling_delay.ino','r+');
i = 1;
tline = fgetl(fid);
A{i} = tline;
while ischar(tline)
    i = i+1;
    tline = fgetl(fid);
    A{i} = tline;
end
fclose(fid);



%% Calculating SOS

% The scaling constant, edit to alter the precision of the filter
% coefficients
divisor = 10000;

% Defines the length of the delay line.
N = 32768;

% Sets the sampling frequency to calculate the delay in samples
fs = 44100;
%% Formating attenuation factor

% Scales the coefficients
alpha = round(a*divisor);

% Calculates the delay in samples
delay_samples = delay_t*fs;
%% Writing the parameters
A{24} = sprintf('#define m %d',N);
A{26} = sprintf('#define n %d',N-1);
A{28} = sprintf('int dlay_max = %d;',delay_samples);
A{31} = sprintf('int alpha = %d;',alpha);
A{32} = sprintf('int divisor = %d;',divisor);

% Stores the filter coefficients to one array with the Arduino formatting.
fid = fopen('./../arduinoFilterArduinoCode/Const_sampling_delayRUN/Const_sampling_delayRUN.ino', 'w');
for i = 1:numel(A)
    if A{i+1} == -1
        fprintf(fid,'%s', A{i});
        break
    else
        fprintf(fid,'%s\n', A{i});
    end
end
% The filter coefficients are as strings that can be compiled by the Arduino
% IDE.
fclose('all');

%% Launch Arduino IDE and uplode code to Ardiono

% This part enables the Matlab script to automatly upload the skecth to the
% Arduino. Work on Pc and MAC.
a = pwd;
if (ispc)     
    A = ['START ' a '\..\arduinoFilterArduinoCode\Const_sampling_delayRUN\Const_sampling_delayRUN.ino'];
    dos(A);
    
    pause(3)
    robot.keyPress(KeyEvent.VK_CONTROL);
    robot.keyPress(KeyEvent.VK_U);
    robot.keyRelease(KeyEvent.VK_CONTROL);
    robot.keyRelease(KeyEvent.VK_U);
else (ismac)
    A = ['open ./../arduinoFilterArduinoCode/Const_sampling_delayRUN/Const_sampling_delayRUN.ino'];
    system(A);
  
     pause(3)
    robot.keyPress(KeyEvent.VK_META);
    robot.keyPress(KeyEvent.VK_U);
    robot.keyRelease(KeyEvent.VK_META);
    robot.keyRelease(KeyEvent.VK_U);
end


end