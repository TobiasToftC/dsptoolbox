

#ifdef __cplusplus
extern "C" 
{
#endif

// Circular buffer, power of two.
#define n 16384
// BUFMASK = n-1
#define m 16383
short w[n] = {0}; 
short sin_ref[n];
float alpha = 0.5;
int divisor = 10000;
int j; 
int led = 13;

void ADC_Handler (void)
{

  if (ADC->ADC_ISR & ADC_ISR_EOC7)   // ensure there was an End-of-Conversion and we read the ISR reg
  {
    int val = *(ADC->ADC_CDR+7) ;    // get conversion result
    
  //*********************************
    PIO_Clear(PIOB,PIO_PB27B_TIOB0);
    
    // LED warning
      if (val >= 4095) {
        PIO_Set(PIOB,PIO_PB25B_TIOA0);
      } else {
        PIO_Clear(PIOB,PIO_PB25B_TIOA0);
      }

      // Scales the input to vary between -2047 and 2047
      w[j] = val-2047;
 
        
      w[j] = ((divisor-(alpha*divisor))*w[j])/divisor + ((alpha*divisor)*w[(n-1+j-sin_ref[j])%n])/divisor;
      
  PIO_Set(PIOB,PIO_PB27B_TIOB0);
  
  //*********************************  
    val = (w[j]+2047);           
    j = (j+1) & m ;      // move pointer_ j = 1000000000 & 0111111111 = 0 (1024 & 1023)
    analogWriteResolution(12);
    analogWrite(DAC0,val);
  }

  
}
#ifdef __cplusplus
}
#endif

void setup()
{
  pinMode(led,OUTPUT); 
  // The SAM3X8E CPU has 3 Timer Counters (TC) they are called TC0, TC1, TC2.
  // Every Timer Counter contains 3 Channels numbered 0, 1 and 2 (this give us a total of 9 Channels).
  // Every Channel has its own counters and interrupt handler that are independent from other Channels.
  adc_setup () ;   
  
  pmc_enable_periph_clk(TC_INTERFACE_ID) ;  // Enable clock

  TC0->TC_CHANNEL[0].TC_CCR = TC_CCR_CLKDIS ;  // disable internal clocking while setup regs
  TC0->TC_CHANNEL[0].TC_IDR = 0xFFFFFFFF ;     // disable interrupts
  TC0->TC_CHANNEL[0].TC_SR ;                   // read int status reg to clear pending
  
  TC0->TC_CHANNEL[0].TC_CMR = 0x6C000;
              // Clock Selection:     Use TCLK1 (prescale by 2, = 42MHz)
              // Waveform mode:       (Needed for the Trigger selection)
              //                      The TC channel generates one or two PWM signals with the 
              //                      same frequency and independently programmable duty cycles
              // Waveform Selection:  Count-up PWM using RC as threshold
              // RA/RC Compare Effect on TIOA: RA=CLEAR, RC=SET
  
  TC0->TC_CHANNEL[0].TC_RC =  875;     // counter resets on RC, so sets period in terms of 42MHz clock
  TC0->TC_CHANNEL[0].TC_RA =  440;     // Ref. value
  
  //TC0->TC_CHANNEL[0].TC_CCR = TC_CCR_CLKEN | TC_CCR_SWTRG;  // re-enable clocking and switch to hardware trigger source.
  TC0->TC_CHANNEL[0].TC_CCR = 0x5;

    // Sine wave to vary the delay line
  for(int t=0; t<n ;t++){
    sin_ref[t] = (1.6*sin((t*1.9175e-04)))*10000;
    sin_ref[t] = sin_ref[t]/200;
  }

}

void adc_setup ()
{
  NVIC_EnableIRQ (ADC_IRQn);   // Enables and sets up the ADC interrupt vector (ID: ADC_IRQn)
  ADC->ADC_IDR = 0xFFFFFFFF;   // Disable all ADC Register interrupt 
  ADC->ADC_IER = 0x80;         // Enable AOC7 End-Of-Conv interrupt (Pin A0)
  ADC->ADC_CHDR = 0xFFFF;      // Disable all analog pins (A0-A15)
  ADC->ADC_CHER = 0xC0;        // Enable only A0 and A1

  ADC->ADC_CGR = 0x15555555;   // All gains set to mode 1 (gain 1)
//  ADC->ADC_COR = 0x00000000;   // All offsets off

  ADC->ADC_MR = 0x40C00003; // Conversion Trigger
  //TRGSEL: Hardware selection -> TIOA Timer Counter channel 0 
  //TRGEN: The selected hardware (Timer Counter) trigger is enabled               
}

void loop(){
  }
