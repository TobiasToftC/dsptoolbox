//Const_sampling_sketch:   The basis for FIR and IIR filter. 
//    Must be used with compatible hardware.
//    Be carefull not to change anything but the three variables listed below.
//
//   USAGE
//   Define the varibles:
//    Use the Matlab script "Write_to_arduino.m", "ArduinoFilterIIR.m" or "ArduinoFilterFIR.m"
//    To change the filter.
//
//   Authors  :  Mikkel Heber Hahn Petersen - s123028@student.dtu.dk
//              Tobias Toft Christensen - s123028@student.dtu.dk
//              Technical University of Denmark (DTU)
//
//   History :
//   v.1.0   28/01/2016
//   ***********************************************************************

#ifdef __cplusplus
extern "C" 
{
#endif

// Circular buffer, power of two.
#define m 1024
// BUFMASK = m-1
#define n 1023
volatile int b[30] = {154,309,154,361,721,361,351,701,351,350,700,350,353,707,353,359,718,359,365,729,365,363,727,363,319,638,319,937,1874,937};
volatile int a[30] = {10000,-12879,4152,10000,-12972,4254,10000,-13161,4462,10000,-13448,4777,10000,-13839,5207,10000,-14340,5758,10000,-14960,6439,10000,-15709,7262,10000,-16595,8235,10000,-17627,9370};
int w[m];
int x[m];
int x1[m];
int x2[m];
int x3[m];
int x4[m];
int x5[m];
int x6[m];
int x7[m];
int x8[m];
int x9[m];
int x10[m];
int divisor = 10000;
 int j = 0 ;
int val_out;
int led_led = 2;
int led_out = 3;
void ADC_Handler (void)
{
 if (ADC->ADC_ISR & ADC_ISR_EOC7)   // ensure there was an End-of-Conversion and we read the ISR reg
  {
    int val_in = *(ADC->ADC_CDR+7) ;    // get conversion result
    
  //*********************************
    PIO_Clear(PIOB,PIO_PB27B_TIOB0);    // Sampling check - digital pin 13

  x[j] = val_in-2047; 
  
  
x1[j] = (b[0]*x[j] + b[1]*x[(m+j-1)%m] + b[2]*x[(m+j-2)%m] - a[1]*x1[(m+j-1)%m] - a[2]*x1[(m+j-2)%m])/divisor;
x2[j] = (b[3]*x1[j] + b[4]*x1[(m+j-1)%m] + b[5]*x1[(m+j-2)%m] - a[4]*x2[(m+j-1)%m] - a[5]*x2[(m+j-2)%m])/divisor;
x3[j] = (b[6]*x2[j] + b[7]*x2[(m+j-1)%m] + b[8]*x2[(m+j-2)%m] - a[7]*x3[(m+j-1)%m] - a[8]*x3[(m+j-2)%m])/divisor;
x4[j] = (b[9]*x3[j] + b[10]*x3[(m+j-1)%m] + b[11]*x3[(m+j-2)%m] - a[10]*x4[(m+j-1)%m] - a[11]*x4[(m+j-2)%m])/divisor;
x5[j] = (b[12]*x4[j] + b[13]*x4[(m+j-1)%m] + b[14]*x4[(m+j-2)%m] - a[13]*x5[(m+j-1)%m] - a[14]*x5[(m+j-2)%m])/divisor;
x6[j] = (b[15]*x5[j] + b[16]*x5[(m+j-1)%m] + b[17]*x5[(m+j-2)%m] - a[16]*x6[(m+j-1)%m] - a[17]*x6[(m+j-2)%m])/divisor;
x7[j] = (b[18]*x6[j] + b[19]*x6[(m+j-1)%m] + b[20]*x6[(m+j-2)%m] - a[19]*x7[(m+j-1)%m] - a[20]*x7[(m+j-2)%m])/divisor;
x8[j] = (b[21]*x7[j] + b[22]*x7[(m+j-1)%m] + b[23]*x7[(m+j-2)%m] - a[22]*x8[(m+j-1)%m] - a[23]*x8[(m+j-2)%m])/divisor;
x9[j] = (b[24]*x8[j] + b[25]*x8[(m+j-1)%m] + b[26]*x8[(m+j-2)%m] - a[25]*x9[(m+j-1)%m] - a[26]*x9[(m+j-2)%m])/divisor;
x10[j] = (b[27]*x9[j] + b[28]*x9[(m+j-1)%m] + b[29]*x9[(m+j-2)%m] - a[28]*x10[(m+j-1)%m] - a[29]*x10[(m+j-2)%m])/divisor;
val_out = x10[j];

            
  PIO_Set(PIOB,PIO_PB27B_TIOB0);      // Sampling check - digital pin 13
  
  //*********************************  
              
    j = (j+1) & n ;      // move pointer_ j = 1000000000 & 0111111111 = 0 (1024 & 1023)
    
    analogWriteResolution(12);
      analogWrite(DAC0,val_out+2047);

      if (val_in >= 4095) { // LED warning Intput - digital pin number 2
        PIO_Set(PIOB,PIO_PB25B_TIOA0);
        //digitalWrite(led_led,HIGH);
      } else {
        //digitalWrite(led_led,LOW);
        PIO_Clear(PIOB,PIO_PB25B_TIOA0);
      }

      // LED warning output - digital pin number 3
      if (val_out+2047 >= 4090) {
       // PIO_Set(PIOC,PIO_PC28B_TIOA7);
       digitalWrite(led_out,HIGH);
      } else {
      //  PIO_Clear(PIOB,PIO_PC28B_TIOA7);
      digitalWrite(led_out,LOW);
      }
  }

  
}
#ifdef __cplusplus
}
#endif


int led = 13;

void setup()
{
  pinMode(led,OUTPUT);
  pinMode(led_led,OUTPUT);
  pinMode(led_out,OUTPUT);
  // The SAM3X8E CPU has 3 Timer Counters (TC) they are called TC0, TC1, TC2.
  // Every Timer Counter contains 3 Channels numbered 0, 1 and 2 (this give us a total of 9 Channels).
  // Every Channel has its own counters and interrupt handler that are independent from other Channels.
  adc_setup () ;   
  
  pmc_enable_periph_clk(TC_INTERFACE_ID) ;  // Enable clock

  TC0->TC_CHANNEL[0].TC_CCR = TC_CCR_CLKDIS ;  // disable internal clocking while setup regs
  TC0->TC_CHANNEL[0].TC_IDR = 0xFFFFFFFF ;     // disable interrupts
  TC0->TC_CHANNEL[0].TC_SR ;                   // read int status reg to clear pending
  
  TC0->TC_CHANNEL[0].TC_CMR = 0x6C000;
              // Clock Selection:     Use TCLK1 (prescale by 2, = 42MHz)
              // Waveform mode:       (Needed for the Trigger selection) 
              // - The TC channel generates one or two PWM signals with the same frequency and independently programmable duty cycles
              
              // Waveform Selection:  Count-up PWM using RC as threshold
              // RA/RC Compare Effect on TIOA: RA=CLEAR, RC=SET
  
  TC0->TC_CHANNEL[0].TC_RC =  952;     // counter resets on RC, so sets period in terms of 42MHz clock
  TC0->TC_CHANNEL[0].TC_RA =  475;     // Ref. value
  
  //TC0->TC_CHANNEL[0].TC_CCR = TC_CCR_CLKEN | TC_CCR_SWTRG;  // re-enable clocking and switch to hardware trigger source.
  TC0->TC_CHANNEL[0].TC_CCR = 0x5;
}

void adc_setup ()
{
  NVIC_EnableIRQ (ADC_IRQn);   // Enables and sets up the ADC interrupt vector (ID: ADC_IRQn)
  ADC->ADC_IDR = 0xFFFFFFFF;   // Disable all ADC Register interrupt 
  ADC->ADC_IER = 0x80;         // Enable AOC7 End-Of-Conv interrupt (Pin A0)
  ADC->ADC_CHDR = 0xFFFF;      // Disable all analog pins (A0-A15)
  ADC->ADC_CHER = 0xC0;        // Enable only A0 and A1

  ADC->ADC_CGR = 0x15555555;   // All gains set to mode 1 (gain 1)
//  ADC->ADC_COR = 0x00000000;   // All offsets off

  ADC->ADC_MR = 0x40C00003; // Conversion Trigger
  //TRGSEL: Hardware selection -> TIOA Timer Counter channel 0 
  //TRGEN: The selected hardware (Timer Counter) trigger is enabled               
}

void loop(){
  }