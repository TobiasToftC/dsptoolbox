//Const_sampling_delay:   Implements a recursive delay filter on the DSP-toolbox for DTU course 31606.
//    Must be used with compatible hardware.
//    Be carefull not to change anything but the three variables listed below.
//
//   USAGE
//   Define the varibles:
//    Use the Matlab script "Write_to_arduino.m" or "ArduinoFilter_delay.m"
//    To change the filter.
//
//   Authors  :  Mikkel Heber Hahn Petersen - s123028@student.dtu.dk
//              Tobias Toft Christensen - s123028@student.dtu.dk
//              Technical University of Denmark (DTU)
//
//   History :
//   v.1.0   28/01/2016
//   ***********************************************************************

#ifdef __cplusplus
extern "C"
{
#endif

// Circular buffer, power of two.
#define m 32768
// BUFMASK = n-1
#define n 32767
short w[n] = {0};       // Delay line
int dlay_max = 22050;
int dlay;               // Actual delay in samples
int dlayRead;           // Used to read the potentiometer
int alpha = 5000;
int divisor = 10000;
int j;                  // Ring buffer counter
int samplingCheck = 13; // Sampling frequency digital pin number x
int led = 2;            // Led warning: digital pin number x
int led_out = 3;        // Led warning: digital pin number x

void ADC_Handler (void)  //Interrupt handler - A program that is passed when an interrupt occurs.
{


  if (ADC->ADC_ISR & ADC_ISR_EOC7)   // ensure there was an End-of-Conversion and we read the ISR reg
  {
    int val = *(ADC->ADC_CDR + 7) ;  // get conversion result

    //*********************************
    PIO_Clear(PIOB, PIO_PB27B_TIOB0);   // Sampling check - digital pin 13

    // LED warning Intput - digital pin number 2
    if (val >= 4095) {
      PIO_Set(PIOB, PIO_PB25B_TIOA0);
    } else {
      PIO_Clear(PIOB, PIO_PB25B_TIOA0);
    }



    // Scales the input to vary between -2047 and 2047
    w[j] = val - 2047;

    // Control delay time with Potentiometer
    /* int d = *(ADC->ADC_CDR+6) ; //AnalogRead
       dlayRead = d/100;
       dlay = ((dlay_max*dlayRead*100)/4095);*/
    dlay = dlay_max;

    // Scales the calculation so the input + the delay wil have an amplitude of unity.
    // (1-a)*input + a*delay, is is to avoid clipping. 
    w[j] = ((divisor - (alpha)) * w[j]) / divisor  + ((alpha) * w[(n + j - dlay) % n]) / divisor;

    PIO_Set(PIOB, PIO_PB27B_TIOB0); // Sampling check - digital pin 13

    //*********************************
    val = (w[j] + 2047);  // Scales output to range 0-4095

    j = (j + 1) & n ;
    // move pointer_ j = 1000000000 & 0111111111 = 0 (1024 & 1023)

    analogWriteResolution(12);
    analogWrite(DAC0, val);

    // LED warning Output
    if (val >= 4095) {
       PIO_Set(PIOC,PIO_PC28B_TIOA7);
      digitalWrite(led_out, HIGH);
    } else {
        PIO_Clear(PIOB,PIO_PC28B_TIOA7);
      digitalWrite(led_out, LOW);
    }
  }

}
#ifdef __cplusplus
}
#endif

void setup()
{
  pinMode(led, OUTPUT);
  pinMode(led_out, OUTPUT);
  pinMode(samplingCheck, OUTPUT);
  // The SAM3X8E CPU has 3 Timer Counters (TC) they are called TC0, TC1, TC2.
  // Every Timer Counter contains 3 Channels numbered 0, 1 and 2 (this give us a total of 9 Channels).
  // Every Channel has its own counters and interrupt handler that are independent from other Channels.
  adc_setup () ;

  pmc_enable_periph_clk(TC_INTERFACE_ID) ;  // Enable clock

  TC0->TC_CHANNEL[0].TC_CCR = TC_CCR_CLKDIS ;  // disable internal clocking while setup regs
  TC0->TC_CHANNEL[0].TC_IDR = 0xFFFFFFFF ;     // disable interrupts
  TC0->TC_CHANNEL[0].TC_SR ;                   // read int status reg to clear pending

  TC0->TC_CHANNEL[0].TC_CMR = 0x6C000;
  // Clock Selection:     Use TCLK1 (prescale by 2, = 42MHz)
  // Waveform mode:       (Needed for the Trigger selection)
  //                      The TC channel generates one or two PWM signals with the
  //                      same frequency and independently programmable duty cycles
  // Waveform Selection:  Count-up PWM using RC as threshold
  // RA/RC Compare Effect on TIOA: RA=CLEAR, RC=SET

  TC0->TC_CHANNEL[0].TC_RC =  875;     // counter resets on RC, so sets period in terms of 42MHz clock
  TC0->TC_CHANNEL[0].TC_RA =  440;     // Ref. value

  //TC0->TC_CHANNEL[0].TC_CCR = TC_CCR_CLKEN | TC_CCR_SWTRG;  // re-enable clocking and switch to hardware trigger source.
  TC0->TC_CHANNEL[0].TC_CCR = 0x5;
}

void adc_setup ()
{
  NVIC_EnableIRQ (ADC_IRQn);   // Enables and sets up the ADC interrupt vector (ID: ADC_IRQn)
  ADC->ADC_IDR = 0xFFFFFFFF;   // Disable all ADC Register interrupt
  ADC->ADC_IER = 0x80;         // Enable AOC7 End-Of-Conv interrupt (Pin A0)
  ADC->ADC_CHDR = 0xFFFF;      // Disable all analog pins (A0-A15)
  ADC->ADC_CHER = 0xC0;        // Enable only A0 and A1

  ADC->ADC_CGR = 0x15555555;   // All gains set to mode 1 (gain 1)
  //  ADC->ADC_COR = 0x00000000;   // All offsets off

  ADC->ADC_MR = 0x40C00003; // Conversion Trigger
  //TRGSEL: Hardware selection -> TIOA Timer Counter channel 0
  //TRGEN: The selected hardware (Timer Counter) trigger is enabled
}

void loop() {
}