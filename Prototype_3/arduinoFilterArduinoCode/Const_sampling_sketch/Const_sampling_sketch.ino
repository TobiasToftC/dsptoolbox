//Const_sampling_sketch:   The basis for FIR and IIR filter. 
//    Must be used with compatible hardware.
//    Be carefull not to change anything but the three variables listed below.
//
//   USAGE
//   Define the varibles:
//    Use the Matlab script "Write_to_arduino.m", "ArduinoFilterIIR.m" or "ArduinoFilterFIR.m"
//    To change the filter.
//
//   Authors  :  Mikkel Heber Hahn Petersen - s123028@student.dtu.dk
//              Tobias Toft Christensen - s123028@student.dtu.dk
//              Technical University of Denmark (DTU)
//
//   History :
//   v.1.0   28/01/2016
//   ***********************************************************************

#ifdef __cplusplus
extern "C" 
{
#endif

// Circular buffer, power of two.
#define m 1024
// BUFMASK = m-1
#define n 1023
volatile int b[3] = {7811,-13862,7811};

int w[m];
int x[m];
int divisor = 10000 ;
 int j = 0 ;
int val_out;
int led_led = 2;
int led_out = 3;
void ADC_Handler (void)
{
 if (ADC->ADC_ISR & ADC_ISR_EOC7)   // ensure there was an End-of-Conversion and we read the ISR reg
  {
    int val_in = *(ADC->ADC_CDR+7) ;    // get conversion result
    
  //*********************************
    PIO_Clear(PIOB,PIO_PB27B_TIOB0);    // Sampling check - digital pin 13

  x[j] = val_in-2047; 
  
  

            
  PIO_Set(PIOB,PIO_PB27B_TIOB0);      // Sampling check - digital pin 13
  
  //*********************************  
              
    j = (j+1) & n ;      // move pointer_ j = 1000000000 & 0111111111 = 0 (1024 & 1023)
    
    analogWriteResolution(12);
      analogWrite(DAC0,val_out+2047);

      if (val_in >= 4095) { // LED warning Intput - digital pin number 2
        PIO_Set(PIOB,PIO_PB25B_TIOA0);
        //digitalWrite(led_led,HIGH);
      } else {
        //digitalWrite(led_led,LOW);
        PIO_Clear(PIOB,PIO_PB25B_TIOA0);
      }

      // LED warning output - digital pin number 3
      if (val_out+2047 >= 4090) {
       // PIO_Set(PIOC,PIO_PC28B_TIOA7);
       digitalWrite(led_out,HIGH);
      } else {
      //  PIO_Clear(PIOB,PIO_PC28B_TIOA7);
      digitalWrite(led_out,LOW);
      }
  }

  
}
#ifdef __cplusplus
}
#endif


int led = 13;

void setup()
{
  pinMode(led,OUTPUT);
  pinMode(led_led,OUTPUT);
  pinMode(led_out,OUTPUT);
  // The SAM3X8E CPU has 3 Timer Counters (TC) they are called TC0, TC1, TC2.
  // Every Timer Counter contains 3 Channels numbered 0, 1 and 2 (this give us a total of 9 Channels).
  // Every Channel has its own counters and interrupt handler that are independent from other Channels.
  adc_setup () ;   
  
  pmc_enable_periph_clk(TC_INTERFACE_ID) ;  // Enable clock

  TC0->TC_CHANNEL[0].TC_CCR = TC_CCR_CLKDIS ;  // disable internal clocking while setup regs
  TC0->TC_CHANNEL[0].TC_IDR = 0xFFFFFFFF ;     // disable interrupts
  TC0->TC_CHANNEL[0].TC_SR ;                   // read int status reg to clear pending
  
  TC0->TC_CHANNEL[0].TC_CMR = 0x6C000;
              // Clock Selection:     Use TCLK1 (prescale by 2, = 42MHz)
              // Waveform mode:       (Needed for the Trigger selection) 
              // - The TC channel generates one or two PWM signals with the same frequency and independently programmable duty cycles
              
              // Waveform Selection:  Count-up PWM using RC as threshold
              // RA/RC Compare Effect on TIOA: RA=CLEAR, RC=SET
  
  TC0->TC_CHANNEL[0].TC_RC =  952;     // counter resets on RC, so sets period in terms of 42MHz clock
  TC0->TC_CHANNEL[0].TC_RA =  475;     // Ref. value
  
  //TC0->TC_CHANNEL[0].TC_CCR = TC_CCR_CLKEN | TC_CCR_SWTRG;  // re-enable clocking and switch to hardware trigger source.
  TC0->TC_CHANNEL[0].TC_CCR = 0x5;
}

void adc_setup ()
{
  NVIC_EnableIRQ (ADC_IRQn);   // Enables and sets up the ADC interrupt vector (ID: ADC_IRQn)
  ADC->ADC_IDR = 0xFFFFFFFF;   // Disable all ADC Register interrupt 
  ADC->ADC_IER = 0x80;         // Enable AOC7 End-Of-Conv interrupt (Pin A0)
  ADC->ADC_CHDR = 0xFFFF;      // Disable all analog pins (A0-A15)
  ADC->ADC_CHER = 0xC0;        // Enable only A0 and A1

  ADC->ADC_CGR = 0x15555555;   // All gains set to mode 1 (gain 1)
//  ADC->ADC_COR = 0x00000000;   // All offsets off

  ADC->ADC_MR = 0x40C00003; // Conversion Trigger
  //TRGSEL: Hardware selection -> TIOA Timer Counter channel 0 
  //TRGEN: The selected hardware (Timer Counter) trigger is enabled               
}

void loop(){
  }
