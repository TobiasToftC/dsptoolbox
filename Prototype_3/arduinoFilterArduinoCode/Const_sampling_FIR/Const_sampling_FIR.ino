//Const_sampling_sketch:   The basis for FIR and IIR filter. 
//    Must be used with compatible hardware.
//    Be carefull not to change anything but the three variables listed below.
//
//   USAGE
//   Define the varibles:
//    Use the Matlab script "Write_to_arduino.m", "ArduinoFilterIIR.m" or "ArduinoFilterFIR.m"
//    To change the filter.
//
//   Authors  :  Mikkel Heber Hahn Petersen - s123028@student.dtu.dk
//              Tobias Toft Christensen - s123028@student.dtu.dk
//              Technical University of Denmark (DTU)
//
//   History :
//   v.1.0   28/01/2016
//   ***********************************************************************

#ifdef __cplusplus
extern "C" 
{
#endif

// Circular buffer, power of two.
#define m 128
// BUFMASK = m-1
#define n 127
volatile int b[117] = {36,-92,80,196,-608,679,-416,41,352,-673,792,-478,-22,239,-241,176,-74,-26,-10,374,-681,533,-137,-317,729,-996,875,-222,-382,554,-515,358,-135,-75,-60,592,-751,319,314,-967,1481,-1650,927,495,-1384,1526,-1289,735,27,-609,-82,1426,-1231,-526,3232,-6983,12297,-19342,23497,-19342,12297,-6983,3232,-526,-1231,1426,-82,-609,27,735,-1289,1526,-1384,495,927,-1650,1481,-967,314,319,-751,592,-60,-75,-135,358,-515,554,-382,-222,875,-996,729,-317,-137,533,-681,374,-10,-26,-74,176,-241,239,-22,-478,792,-673,352,41,-416,679,-608,196,80,-92,36};

int w[m];
int x[m];
int divisor = 10000;
 int j = 0 ;
int val_out;
int led_led = 2;
int led_out = 3;
void ADC_Handler (void)
{
 if (ADC->ADC_ISR & ADC_ISR_EOC7)   // ensure there was an End-of-Conversion and we read the ISR reg
  {
    int val_in = *(ADC->ADC_CDR+7) ;    // get conversion result
    
  //*********************************
    PIO_Clear(PIOB,PIO_PB27B_TIOB0);    // Sampling check - digital pin 13

  x[j] = val_in-2047; 
  
  val_out = (36*x[j] + -92*x[(m+j-1)%m] + 80*x[(m+j-2)%m] + 196*x[(m+j-3)%m] + -608*x[(m+j-4)%m] + 679*x[(m+j-5)%m] + -416*x[(m+j-6)%m] + 41*x[(m+j-7)%m] + 352*x[(m+j-8)%m] + -673*x[(m+j-9)%m] + 792*x[(m+j-10)%m] + -478*x[(m+j-11)%m] + -22*x[(m+j-12)%m] + 239*x[(m+j-13)%m] + -241*x[(m+j-14)%m] + 176*x[(m+j-15)%m] + -74*x[(m+j-16)%m] + -26*x[(m+j-17)%m] + -10*x[(m+j-18)%m] + 374*x[(m+j-19)%m] + -681*x[(m+j-20)%m] + 533*x[(m+j-21)%m] + -137*x[(m+j-22)%m] + -317*x[(m+j-23)%m] + 729*x[(m+j-24)%m] + -996*x[(m+j-25)%m] + 875*x[(m+j-26)%m] + -222*x[(m+j-27)%m] + -382*x[(m+j-28)%m] + 554*x[(m+j-29)%m] + -515*x[(m+j-30)%m] + 358*x[(m+j-31)%m] + -135*x[(m+j-32)%m] + -75*x[(m+j-33)%m] + -60*x[(m+j-34)%m] + 592*x[(m+j-35)%m] + -751*x[(m+j-36)%m] + 319*x[(m+j-37)%m] + 314*x[(m+j-38)%m] + -967*x[(m+j-39)%m] + 1481*x[(m+j-40)%m] + -1650*x[(m+j-41)%m] + 927*x[(m+j-42)%m] + 495*x[(m+j-43)%m] + -1384*x[(m+j-44)%m] + 1526*x[(m+j-45)%m] + -1289*x[(m+j-46)%m] + 735*x[(m+j-47)%m] + 27*x[(m+j-48)%m] + -609*x[(m+j-49)%m] + -82*x[(m+j-50)%m] + 1426*x[(m+j-51)%m] + -1231*x[(m+j-52)%m] + -526*x[(m+j-53)%m] + 3232*x[(m+j-54)%m] + -6983*x[(m+j-55)%m] + 12297*x[(m+j-56)%m] + -19342*x[(m+j-57)%m] + 23497*x[(m+j-58)%m] + -19342*x[(m+j-59)%m] + 12297*x[(m+j-60)%m] + -6983*x[(m+j-61)%m] + 3232*x[(m+j-62)%m] + -526*x[(m+j-63)%m] + -1231*x[(m+j-64)%m] + 1426*x[(m+j-65)%m] + -82*x[(m+j-66)%m] + -609*x[(m+j-67)%m] + 27*x[(m+j-68)%m] + 735*x[(m+j-69)%m] + -1289*x[(m+j-70)%m] + 1526*x[(m+j-71)%m] + -1384*x[(m+j-72)%m] + 495*x[(m+j-73)%m] + 927*x[(m+j-74)%m] + -1650*x[(m+j-75)%m] + 1481*x[(m+j-76)%m] + -967*x[(m+j-77)%m] + 314*x[(m+j-78)%m] + 319*x[(m+j-79)%m] + -751*x[(m+j-80)%m] + 592*x[(m+j-81)%m] + -60*x[(m+j-82)%m] + -75*x[(m+j-83)%m] + -135*x[(m+j-84)%m] + 358*x[(m+j-85)%m] + -515*x[(m+j-86)%m] + 554*x[(m+j-87)%m] + -382*x[(m+j-88)%m] + -222*x[(m+j-89)%m] + 875*x[(m+j-90)%m] + -996*x[(m+j-91)%m] + 729*x[(m+j-92)%m] + -317*x[(m+j-93)%m] + -137*x[(m+j-94)%m] + 533*x[(m+j-95)%m] + -681*x[(m+j-96)%m] + 374*x[(m+j-97)%m] + -10*x[(m+j-98)%m] + -26*x[(m+j-99)%m] + -74*x[(m+j-100)%m] + 176*x[(m+j-101)%m] + -241*x[(m+j-102)%m] + 239*x[(m+j-103)%m] + -22*x[(m+j-104)%m] + -478*x[(m+j-105)%m] + 792*x[(m+j-106)%m] + -673*x[(m+j-107)%m] + 352*x[(m+j-108)%m] + 41*x[(m+j-109)%m] + -416*x[(m+j-110)%m] + 679*x[(m+j-111)%m] + -608*x[(m+j-112)%m] + 196*x[(m+j-113)%m] + 80*x[(m+j-114)%m] + -92*x[(m+j-115)%m] + 36*x[(m+j-116)%m])/divisor;

            
  PIO_Set(PIOB,PIO_PB27B_TIOB0);      // Sampling check - digital pin 13
  
  //*********************************  
              
    j = (j+1) & n ;      // move pointer_ j = 1000000000 & 0111111111 = 0 (1024 & 1023)
    
    analogWriteResolution(12);
      analogWrite(DAC0,val_out+2047);

      if (val_in >= 4095) { // LED warning Intput - digital pin number 2
        PIO_Set(PIOB,PIO_PB25B_TIOA0);
        //digitalWrite(led_led,HIGH);
      } else {
        //digitalWrite(led_led,LOW);
        PIO_Clear(PIOB,PIO_PB25B_TIOA0);
      }

      // LED warning output - digital pin number 3
      if (val_out+2047 >= 4090) {
       // PIO_Set(PIOC,PIO_PC28B_TIOA7);
       digitalWrite(led_out,HIGH);
      } else {
      //  PIO_Clear(PIOB,PIO_PC28B_TIOA7);
      digitalWrite(led_out,LOW);
      }
  }

  
}
#ifdef __cplusplus
}
#endif


int led = 13;

void setup()
{
  pinMode(led,OUTPUT);
  pinMode(led_led,OUTPUT);
  pinMode(led_out,OUTPUT);
  // The SAM3X8E CPU has 3 Timer Counters (TC) they are called TC0, TC1, TC2.
  // Every Timer Counter contains 3 Channels numbered 0, 1 and 2 (this give us a total of 9 Channels).
  // Every Channel has its own counters and interrupt handler that are independent from other Channels.
  adc_setup () ;   
  
  pmc_enable_periph_clk(TC_INTERFACE_ID) ;  // Enable clock

  TC0->TC_CHANNEL[0].TC_CCR = TC_CCR_CLKDIS ;  // disable internal clocking while setup regs
  TC0->TC_CHANNEL[0].TC_IDR = 0xFFFFFFFF ;     // disable interrupts
  TC0->TC_CHANNEL[0].TC_SR ;                   // read int status reg to clear pending
  
  TC0->TC_CHANNEL[0].TC_CMR = 0x6C000;
              // Clock Selection:     Use TCLK1 (prescale by 2, = 42MHz)
              // Waveform mode:       (Needed for the Trigger selection) 
              // - The TC channel generates one or two PWM signals with the same frequency and independently programmable duty cycles
              
              // Waveform Selection:  Count-up PWM using RC as threshold
              // RA/RC Compare Effect on TIOA: RA=CLEAR, RC=SET
  
  TC0->TC_CHANNEL[0].TC_RC =  952;     // counter resets on RC, so sets period in terms of 42MHz clock
  TC0->TC_CHANNEL[0].TC_RA =  475;     // Ref. value
  
  //TC0->TC_CHANNEL[0].TC_CCR = TC_CCR_CLKEN | TC_CCR_SWTRG;  // re-enable clocking and switch to hardware trigger source.
  TC0->TC_CHANNEL[0].TC_CCR = 0x5;
}

void adc_setup ()
{
  NVIC_EnableIRQ (ADC_IRQn);   // Enables and sets up the ADC interrupt vector (ID: ADC_IRQn)
  ADC->ADC_IDR = 0xFFFFFFFF;   // Disable all ADC Register interrupt 
  ADC->ADC_IER = 0x80;         // Enable AOC7 End-Of-Conv interrupt (Pin A0)
  ADC->ADC_CHDR = 0xFFFF;      // Disable all analog pins (A0-A15)
  ADC->ADC_CHER = 0xC0;        // Enable only A0 and A1

  ADC->ADC_CGR = 0x15555555;   // All gains set to mode 1 (gain 1)
//  ADC->ADC_COR = 0x00000000;   // All offsets off

  ADC->ADC_MR = 0x40C00003; // Conversion Trigger
  //TRGSEL: Hardware selection -> TIOA Timer Counter channel 0 
  //TRGEN: The selected hardware (Timer Counter) trigger is enabled               
}

void loop(){
  }