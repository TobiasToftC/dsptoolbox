//MovingAverage_7:   Implements a moving average filter of order 7.
//    Must be used with the DSP-toolbox for DTU course 31606 or compatible hardware.
//    Be carefull not to change anything but the two variables listed below.
//
//   USAGE
//    d[7]:   Defines the length of delay line and the order of the filter.
//    n:      the order of the filter and length of delay line.
//
//   Authors  :  Mikkel Petersen and Tobias Toft Christensen, © 2016
//              Technical University of Denmark (DTU)
//              s123028@student.dtu.dk
//              s123023@student.dtu.dk
//
//   History :
//   v.1.0   2016/01/28
//   ***********************************************************************

// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

// Audio variables
int incomingAudio ;
int outcomingAudio;

//Variables
int d[7] = {0};
int j = 0;
int n = 7;
int y = 0;
int size;
int tempy = 0;

void setup() {

  // Defines prescale value sbi for setting a bit, cbi for clearing.
  sbi(ADCSRA, ADPS2) ;
  cbi(ADCSRA, ADPS1) ;
  cbi(ADCSRA, ADPS0) ;

  DDRA = 0xFF;        //Data Direction Register port  all outputs
  //    - Port A = Pins 22-29
  for (byte i = 0; i < 8; i++) {
    pinMode(i, OUTPUT); //set digital pins as outputs
  }

  pinMode(A0, INPUT);
}

void loop() {
  incomingAudio = analogRead(A0);//read input from A0
  incomingAudio = floor(incomingAudio / 4); //scale from 10 bit to 8 bit

  // Saves input to delay line
  d[j] = incomingAudio;

  // Part of the ring buffer
  // Checks if the counter variable j is less then the length of the delay line
  if (j < n - 1) {
    j++;
  }
  else {
    j = 0;
  }

  // Calculates the mean of the delay line
  for (int ii = 0; ii < n; ii++) {
    y += d[ii];
  }
  y = floor(y / n);




  //Output
  outcomingAudio = y;

  PORTA = outcomingAudio; //send out DAC
}






