//FS_test:   Use to test the resulting sampling frequency for a given part of code.
//    Must be used with the DSP-toolbox for DTU course 31606 or compatible hardware.
//    Be carefull not to change anything but the code between the *-lines
//
//   USAGE
//   Insert code in the designated area and open the serial monitor.
//
//   Authors  :  Mikkel Petersen and Tobias Toft Christensen, © 2016
//              Technical University of Denmark (DTU)
//              s123028@student.dtu.dk
//              s123023@student.dtu.dk
//
//   History :
//   v.1.0   2016/01/28
//   ***********************************************************************

// Variables for audio
int incomingAudio ;
int outcomingAudio;

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600) ;             // Setup serial port
  Serial.print("ADC_TEST: ") ;     // Formatting
  start = millis() ;               // Initial time stamp
  for (i = 0 ; i < 1000 ; i++) {

    // Insert code to test processing time:
    // *************************************

    incomingAudio = analogRead(A0);//read input from A0
    incomingAudio = incomingAudio / 4; //scale from 10 bit to 8 bit

    // Test code
    d[j] = incomingAudio;

    if (j < n - 1) {

      if (j - dlay >= 0) {
        tap = d[j - dlay];
      }
      else {
        tap = d[n - dlay + j];
      }

      j++;
    }
    else {

      tap = d[j - dlay];

      j = 0;
    }

    y = incomingAudio + floor(a * tap);

    y = floor(y / (1 * (1 + a)));

    //Output
    outcomingAudio = y;

    // *************************************
    // End of test code

    delta_t = millis() - start;          // Final time stamp

  }
  // Formatting and calucation of sampling time
  Serial.print(delta_t) ;
  Serial.println("msec(1000 calls)") ;
  Serial.print(delta_t) ;
  Serial.println("usec(1 call)") ;
  Serial.print("Fs=1/") ;
  Serial.print(delta_t) ;
  Serial.print("usec=") ;
  Serial.print(1000000 / delta_t) ;
  Serial.print("Hz") ;
}

void loop() {
  // put your main code here, to run repeatedly:

}
