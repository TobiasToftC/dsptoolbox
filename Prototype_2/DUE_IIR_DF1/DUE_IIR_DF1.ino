//DUE_IIR_DF1:   Implements an second order IIR filter on the direct form 1.
//    Must be used with the DSP-toolbox for DTU course 31606 or compatible hardware. 
//    Be carefull not to change anything but the two variables listed below.
// 
//   USAGE
//   Define the varibles:
//    b_float[3]:   Filter coefficients. Up to 4 digit precision
//    a_float[3]:   Filter coefficients. Up to 4 digit precision
//
//   Authors  :  Mikkel Petersen and Tobias Toft Christensen, © 2016
//              Technical University of Denmark (DTU)
//              s123028@student.dtu.dk
//              s123023@student.dtu.dk
//
//   History :
//   v.1.0   2016/01/28
//   ***********************************************************************

int input = A0;       // Input pin
int led = 13;         // Sampling frequency test pin
int incomingAudio;    // To save incoming signal

//Variables
int x [4] = {0,0,0,0};  // Buffer for input signal
int w [4] = {0,0,0,0};  // Buffer for output signal
int j = 0;            // Counter variable from ring buffer
int n = 3;            // Filter order+1
int y = 0;            // Output variable
float A;

// Filter coeffficients:
float b_float[3] = {1,1,1};
float a_float[3] = {1,1,1};

int divisor = 10000;  // Choose so that all filter coefficients become integers

// Allocating space for filter coefficients in int from.
int b[3];             
int a[3];

void setup(){
  pinMode(input,INPUT);
  pinMode(led,OUTPUT);

  

// Converting filter coefficients to int for faster prossecing
for (int i = 0; i < 3; i++) {
  b[i] = b_float[i]*divisor;
  a[i] = a_float[i]*divisor;
}
  
}

void loop() {

  // Makes the sampling frequency more stable
  while  (true) {

    // Sets pin 13 high and low to test sampling frequency
    PIO_Clear(PIOB,PIO_PB27B_TIOB0);
    PIO_Set(PIOB,PIO_PB27B_TIOB0);

    // Part of the ring buffer.
   

      // Reads the incoming signal
      analogReadResolution(12);
      incomingAudio = analogRead(A0);

      // Scales the input to vary between -2047 and 2047
      x[j] = incomingAudio-2047;
      
      // Direct form 1 implementation of IIR filter
      //w[j] = (b[0]*x[j] + b[1]*x[(n+j-1)%n] + b[2]*x[(n+j-2)%n] - a[1]*w[(n+j-1)%n] - a[2]*w[(n+j-2)%n])/divisor; 

      // Scales the output back to range 0-4095
      y = (w[j]+2047);

      // Writes the output to pin DAC0
      analogWriteResolution(12);
     // analogWrite(DAC0,y);

      // Part of the ring buffer.

    j = (j+1) & n;
   
  }
}

