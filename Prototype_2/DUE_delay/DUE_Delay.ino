//DUE_Delay:   Implements a recursive delay filter on the DSP-toolbox for DTU course 31606.
//    Must be used with compatible hardware. 
//    Be carefull not to change anything but the three variables listed below.
// 
//   USAGE
//   Define the varibles:
//    a:          The attenuation constant. range [0;1] 
//    dlay_max:   Maximum value 15.000. 
//    dlay:       Must be the same as dlay_max.
//
//   Authors  :  Mikkel Petersen and Tobias Toft Christensen, © 2016
//              Technical University of Denmark (DTU)
//              s123028@student.dtu.dk
//              s123023@student.dtu.dk
//
//   History :
//   v.1.0   2016/01/28
//   ***********************************************************************

int input = A0;       // Input pin
int led = 13;         // Sampling frequency test pin
int incomingAudio;    // To save incoming signal

//Variables
int w[20000] = {0};   // Buffer for input signal
int j = 0;            // Counter variable from ring buffer
int n;                // Filter order-1
int dlay_max = 15000; // Delay in samples
int dlay = 15000;     // Initial delay in samples
int dlayRead;         // 
int y = 0;            // Output variable
float a = 0.5;        // Attenuation factor
int divisor = 10;     // Choose so that filter coefficients become integers
int alpha;            // Attenuation factor to int
void setup()
{


  // Calculating size of n
  n = sizeof(w) / sizeof(w[0]);

  alpha = a*divisor;
}

void loop() {
  while  (true) {

    // Sets digital pin 13 high and low to test sampling frequency
    PIO_Clear(PIOB,PIO_PB27B_TIOB0);
    PIO_Set(PIOB,PIO_PB27B_TIOB0);

    // Part of the ring buffer.
    if (j < n) {

      // Reads the incoming signal
      analogReadResolution(12);
      incomingAudio = analogRead(A0);
    
      // Scales the input to vary between -2047 and 2047
      w[j] = incomingAudio-2047;

    
      // Scales the calculation so the input + the delay wil have an amplitude of unity.
      // (1-a)*input + a*delay, is is to avoid clipping.
      w[j] = ((divisor-alpha)*w[j])/divisor + (alpha*w[(n+j-dlay)%n])/divisor;

      // Scales the output back to range 0-4095
      y = (w[j]+2047);

      // Sends the out put to the DAC channel 0
      analogWriteResolution(12);
      analogWrite(DAC0,y);      

    // Part of the ring buffer
    // counts the variable j up, if the condition is true.
      j++;
    } else {
      j = 0;
    }

  }
}

